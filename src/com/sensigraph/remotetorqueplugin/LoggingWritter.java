package com.sensigraph.remotetorqueplugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.os.Environment;

public class LoggingWritter {
	
	private String directory= null;
	
	private FileOutputStream fos = null;
	private OutputStreamWriter osw = null;
	
	public boolean setup() {

		if (isSDCardWritable()) {
			try {

				// SD Card Storage
				File sdCard = Environment.getExternalStorageDirectory();
				directory = sdCard.getAbsolutePath() + "/RemoteTorquePlugin";
				File dir = new File(directory);
				dir.mkdirs();

				File file = new File(dir, "test.txt");

				fos = new FileOutputStream(file);
				osw = new OutputStreamWriter(fos);

				// write the string to the file
				osw.write("test");
				osw.flush();
				osw.close();
				file.delete();
				return true;

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {			
			// SD Card Not Available
		}
		return false;
	}
	
	public String getDir()
	{
		return directory;
	}
	
	// Open logs file
	public void openLogFile(String fname){
		try {
			File file = new File(directory, fname);
			fos = new FileOutputStream(file);
			osw = new OutputStreamWriter(fos);
		} catch (IOException e) {
			osw = null;
			e.printStackTrace();
		}		
	}
	
	// Write to logs file
	public void writeLog(String row){
		try {
			if (osw!=null)
				osw.write(row);
		} catch (IOException e) {
			e.printStackTrace();
		}					
	}
	
	// Close logs file
	public void closeLogFile(){
		try {
			if (osw!=null){
				osw.flush();
				osw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public void deleteAllFiles()
	{
		File root = new File(directory);
		File[] Files = root.listFiles();
		if (Files != null) {
			for (int j = 0; j < Files.length; j++) {
				try {
					Files[j].delete();
				} catch (Exception ex) {

				}
			}
		}		
	}
	
	
	
	
	/* Checks if SD Card is available for read and write */
	public boolean isSDCardWritable() {
		String status = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(status)) {
			return true;
		}
		return false;
	}

	/* Checks if SD Card is available to at least read */
	public boolean isSDCardReadable() {
		String status = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(status)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(status)) {
			return true;
		}
		return false;
	}

}
