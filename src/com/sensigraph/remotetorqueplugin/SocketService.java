package com.sensigraph.remotetorqueplugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

public class SocketService {

	String message = "";
	String replied = "";
	String error = "";
	String info = "";
		
	ServerSocket serverSocket;
	// public Activity activity;
	public Handler messageHandler;
	public BlockingQueue<String> blockingQueue;
		
		
	
	public void runService() {
		Thread socketServerThread = new Thread(new SocketServerThread());
		socketServerThread.start();
	}

	public boolean close() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
				return true;
			} catch (Exception e) {
				//e.printStackTrace();
				passMessage(e.getMessage(), Utils.UIMesasge.ERROR );
				return false;
			}
		}
		return true;
	}
	
	interface MyHandlerInterface {
		void onHandle(String obj, Utils.UIMesasge type);
	}

	MyHandlerInterface myHandler;

	public void setHandlerListener(MyHandlerInterface listener) {
		myHandler = listener;
	}

	protected void passMessage(String msg, Utils.UIMesasge type) {
		if (myHandler != null)
			myHandler.onHandle(msg, type);
	}

	private class SocketServerThread extends Thread {

		static final int SocketServerPORT = 8080;
		int count = 0;

		@Override
		public void run() {
			try {
				final String myIp = getIpAddress(); 
				serverSocket = new ServerSocket(SocketServerPORT);
				String str="Server IP: "
						+ myIp + ", port: "
						+ serverSocket.getLocalPort();
				passMessage(str, Utils.UIMesasge.INFO);

				while (true) {
					Socket socket = serverSocket.accept();
					count++;

					SocketServerReplyThread socketServerReplyThread = new SocketServerReplyThread(
							socket, count);
					socketServerReplyThread.run();

				}
			} catch (final Exception e) {
				passMessage("Thread loop error: "+e.getMessage(), Utils.UIMesasge.ERROR);													
			}
		}

	}

	private class SocketServerReplyThread extends Thread {

		private Socket hostThreadSocket;
		int cnt;

		SocketServerReplyThread(Socket socket, int c) {
			hostThreadSocket = socket;
			cnt = c;
		}

		@Override
		public void run() {
			try{			
				InputStream inputStream = hostThreadSocket.getInputStream();
				OutputStream outputStream = hostThreadSocket.getOutputStream();
				PrintStream printStream = new PrintStream(outputStream);
				
				String str = "";
				BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
                String line = null;
                
                str = in.readLine();
                		
				/*
				byte[] buffer = new byte[4096];
				int read = inputStream.read(buffer, 0, 4096); //This is blocking
                while(read != -1){
                    str += new String(buffer, "UTF-8"); 
                    read = inputStream.read(buffer, 0, 4096); //This is blocking
                }
				*/
                                             
                
                // Send message
                Message msg = messageHandler.obtainMessage();
                msg.what= Utils.MessageType.MSG_COMMAND.toInt();
                msg.obj = str;
                messageHandler.sendMessage(msg);
                
                
                // wait for response                 
                String result = blockingQueue.poll(2*1000, TimeUnit.MILLISECONDS);                
                
				if (result==null)
					result = "{'result': '', 'success': False }";
				printStream.print(result);
				printStream.flush();
				printStream.close();
				
				
				try{
					hostThreadSocket.shutdownInput();
				}
				catch (Exception e) {					
				}
				try{
					hostThreadSocket.shutdownOutput();
				}catch(Exception e){				
				}
				try{
					hostThreadSocket.close();
				}catch(Exception e){				
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				error += "Something wrong! " + e.toString() + "\n";
				passMessage(error, Utils.UIMesasge.ERROR );
			}			
		}

	}

	private String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
						.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
						.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
						ip += ""
								+ inetAddress.getHostAddress() + " ";
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
			ip += "Something Wrong! " + e.toString() + "\n";
		}
		return ip;
	}

}
