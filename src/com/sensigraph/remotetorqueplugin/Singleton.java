package com.sensigraph.remotetorqueplugin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.prowl.torque.remote.ITorqueService;

import com.sensigraph.remotetorqueplugin.SocketService.MyHandlerInterface;















import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;


public class Singleton {
	private static Singleton instance = null;
	public static Context context = null;
	public ITorqueService torqueService = null;
	public LoggingWritter writter = new LoggingWritter();
	public DataRow dataRow = new DataRow();
 
	public final int LIMIT = 10;
	public BlockingQueue<String> blockingQueue = new LinkedBlockingQueue<String>(
			LIMIT);
	
	
	public static boolean logging = false;
	public Calendar loggingCal = Calendar.getInstance();
	public Calendar lastTickCal = Calendar.getInstance();
	public static long recording_duration = 0;

	public long processing_duration_sum = 0;
	public long processing_duration_count = 0;
	public String avgProcessingDuration = "";

	public long update_timestamp = 0;
	public long updates_count = 0;
	public String avgUpdateInterval = "";

	public String currentFilename = "";

	public SocketService socketService=null;

	private Timer updateTimer;	
	private long frames_counter=0;
	
	public static String loggingOn = "Logging started";
	public static String loggingOff = "Logging stopped";

	
	protected Singleton() {
		
		
	}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}

	public void init() {
		if (socketService==null){
			writter.setup();
			setupServer();						
		}
		
		if (updateTimer == null) {
			updateTimer = new Timer();
			updateTimer.schedule(new TimerTask() {
				public void run() {
					update();
				}
			}, 1000, // Delay 1000ms
					200); // Period/Repeat in 200ms
		}
	}
	
	public void setTorqueService(ITorqueService ts){
		if (torqueService!=null)
			return;
		 torqueService=ts;
         dataRow.torqueService = ts;
	}
	
	public void setupServer(){
		if (socketService==null){

			socketService = new SocketService();
			socketService.messageHandler = messageHandler;
			socketService.blockingQueue = blockingQueue;

			socketService.setHandlerListener(new MyHandlerInterface() {

				@Override
				public void onHandle(String msg, Utils.UIMesasge type) {
					passUIMessage(msg, type);					
				}
			});
			socketService.runService();
			passUIMessage("", Utils.UIMesasge.ERROR);
		}else{
			passUIMessage("Server is already running", Utils.UIMesasge.ERROR);
			
		}
		
		
	}
	
	public void destroyServer() {
		if (socketService==null)
			return;
		
		if (socketService.close()){
			socketService = null;
			passUIMessage("", Utils.UIMesasge.INFO);
		}
	}
	
	public void startLogging(String paramString)
	  {
	    currentFilename = paramString;
	    passUIMessage(loggingOn, Utils.UIMesasge.BUTTON_START);
	   
	    loggingCal.setTime(new Date());
	    recording_duration=0;
	    frames_counter = 0L;
	    	    
	    processing_duration_sum = 0;
		processing_duration_count = 0;
		avgProcessingDuration = "";
		
		update_timestamp = 0;
		updates_count = 0;
		avgUpdateInterval = "";
		
	    logging = true;
	    writter.openLogFile(paramString);
	  }

	  public void stopLogging()
	  {
		passUIMessage(loggingOff, Utils.UIMesasge.BUTTON_STOP);	    
	    logging = false;
	    writter.closeLogFile();
	  }
	
	
	
	public void update() {
		if (!logging)
			return;
		if (this.torqueService == null){
			passUIMessage("No torqueService", Utils.UIMesasge.ERROR);
			return;
		}

		try {

			long local_timestamp = System.currentTimeMillis();
			
			
			// policz jak cz�sto jest odpalany update()
			long last_measure_diff = local_timestamp-update_timestamp;
			if(updates_count==0)
				update_timestamp = local_timestamp;
			if (last_measure_diff>2*1000){								
				double d = 1.0 * last_measure_diff / updates_count;
				if (d<10000)
					avgUpdateInterval = "\nInterval: " + Math.round(d) + "ms";				
				updates_count = 0;				
				update_timestamp = local_timestamp;
				Log.i("RemoteTorquePlugin", "update(), recording duration: "+recording_duration);
			}			
			updates_count++;
						
			long diff_ms = Calendar.getInstance().getTimeInMillis()- loggingCal.getTimeInMillis();
			Date localDate1 = new Date(diff_ms);
			recording_duration = (long) (diff_ms / 1000);

			// clear last tick - czas bezczynno�ci
			lastTickCal.setTimeInMillis(local_timestamp);

			StringBuilder localStringBuilder = new StringBuilder(
					String.valueOf(loggingOn)).append(" ");
			String recording_timer = String.format("%02d:%02d:%02d Samples: %d",
					Integer.valueOf(localDate1.getHours())-1,
					Integer.valueOf(localDate1.getMinutes()),
					Integer.valueOf(localDate1.getSeconds()),
					Long.valueOf(frames_counter));
			frames_counter++;

			if (processing_duration_count > 10) {
				double d = 0;
				if (processing_duration_count>0)
					d = 1.0 * processing_duration_sum / processing_duration_count;
				processing_duration_count = 0;
				processing_duration_sum = 0;
				avgProcessingDuration = "\nProcessing time: " + Math.round(d) + "ms";
			}

			String status = recording_timer + avgProcessingDuration+ ", "+avgUpdateInterval;
			Message localMessage = messageHandler.obtainMessage();
			localMessage.what = Utils.MessageType.MSG_UPDATE_STATUS.toInt();
			localMessage.obj = status;
			messageHandler.sendMessage(localMessage);

			String row = dataRow.prepareRow();
			if (row != null) {
				Date localtime = new Date();
				String ts_str = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
						.format(localtime);
				String ts_epoch = Long.toString(localtime.getTime());
				if (writter != null)
					writter.writeLog(ts_str + "," + ts_epoch + "," + row + "\n");
			}else
			{
				int z=0;
			}
			processing_duration_sum= processing_duration_sum + (System.currentTimeMillis() - local_timestamp);
			processing_duration_count++;

		} catch (Exception ex) {
			String msg = ex.getMessage();
			passUIMessage("update error: "+msg, Utils.UIMesasge.ERROR);
		}
	}
	
	
	
	
	public final Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			
			 String data = msg.obj.toString();
			 Utils.RequestData localRequestData = new Utils.RequestData(data);
			 String cmd = localRequestData.cmd;
			 
			 double ts_mils = 1.0*System.currentTimeMillis() / 1000.0;
			 String ts = String.valueOf(ts_mils);
			 String result = "{'result':'', 'success':'False', 'data':'', 'ts' : '" + ts + "' }";
			 
			switch (Utils.MessageType.get(msg.what)) {
			case MSG_COMMAND:							
				if (cmd.equals("ping")){
					result = "{'result':'pong', 'success':'True', 'data':'" + localRequestData.data + "', 'ts' : '" + ts + "' }";					
				}else if (cmd.equals("startLogging")||cmd.equals("start")){
					if (!logging){
												
	                    String fn= new SimpleDateFormat("yyyy_MM_dd_HHmmss'.csv'").format(new Date());
	                    if ((localRequestData.data != null) && (localRequestData.data.length() > 0)) {
	                      fn = localRequestData.data;
	                    }
	                    
	                    startLogging(fn);
	                    
	                    result = "{'result':'"+cmd+"', 'success':'True', 'data':'Running successfully', 'ts' : '" + ts + "' }";										
					}else{
						result = "{'result':'"+cmd+"', 'success':'True', 'data':'Already running', 'ts' : '" + ts + "' }";
					}					
				}else if (cmd.equals("stopLogging")||cmd.equals("stop")){
					if (logging){
						stopLogging();
						result = "{'result':'"+cmd+"', 'success':'True', 'data':'Stopped successfully', 'ts' : '" + ts + "' }";											
					}else{
						result = "{'result':'"+cmd+"', 'success':'True', 'data':'Already stopped', 'ts' : '" + ts + "' }";
					}
					
					
				}else if (cmd.equals("remove_output_files")){
					result = "{'result':'"+cmd+"', 'success':'True', 'data':'', 'ts' : '" + ts + "' }";
					// najpierw odpowiedz, potem kasuj pliki
					pushResultToQueue(data, result);
					writter.deleteAllFiles();
					return;	
					
				}else if (cmd.equals("send_file")){
					
					// wys�anie pliku na zadany adres
					/*
					try{
						new AsyncHttpPostTask(localRequestData.data).execute(new File(localRequestData.arg));
					}catch (Exception e) {
						
					}*/
					
				}else if (cmd.equals("getStatus")){					
                      String status = "False";
                      String extra = "";
                      long last_tick = (Calendar.getInstance().getTimeInMillis() - lastTickCal.getTimeInMillis())/1000;
                      if (logging)
                      {
                    	  status = "True";
                    	  extra = ", 'fname': '" + currentFilename + "' " ;
                      }
                      result = "{"
                      		+ "'result':'"+cmd+"', "+
                    		  "'success':'True', "+
                    		  "'data':'" + status + "', "+
                    		  // "'dummy' : " + 0 +", "+
                    		  "'duration' : " + recording_duration+", "+
                    		  "'last_tick' : "+last_tick+", "+
                    		  "'frames' : " + frames_counter +
                    		  ",  'ts' : '" + ts + "' " + extra + " }";                    					
				}
				pushResultToQueue(data, result);				
				break;
			case MSG_START_RECORDING:
				break;
			case MSG_TEST:
				break;
			case MSG_UPDATE_STATUS:
				passUIMessage(data, Utils.UIMesasge.STATUS);
				break;
			default:
				break;

			}
			
			

		}
	};
	
	private void pushResultToQueue(String data, String result){
		try{
			passUIMessage(data + "\n"+result, Utils.UIMesasge.MESSAGE);
			blockingQueue.put(result);
		}catch(Exception ex){
			passUIMessage("blockingQueue.put() Error", Utils.UIMesasge.ERROR);					
		}
		
	}
	
	
	
	interface UIHandlerInterface {
		void onHandle(String obj, Utils.UIMesasge type);
	}

	UIHandlerInterface uiHandler;

	public void setUIHandlerListener(UIHandlerInterface listener) {
		uiHandler = listener;
	}

	protected void passUIMessage(String msg, Utils.UIMesasge type) {
		if (uiHandler != null)
			uiHandler.onHandle(msg, type);
	}

}
