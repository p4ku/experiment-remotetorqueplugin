package com.sensigraph.remotetorqueplugin;

import org.json.JSONObject;

public class Utils {
	
	public enum UIMesasge {STATUS, INFO, MESSAGE, ERROR, BUTTON_START, BUTTON_STOP};

	public enum MessageType {
		MSG_TEST(1), MSG_START_RECORDING(2), MSG_UPDATE_STATUS(3), MSG_COMMAND(4);
		private int value;

		private MessageType(int value) {
			this.value = value;
		}
		
		public int toInt() {
			return this.value;
		}
		 public static MessageType get(int value){
		        for (MessageType a: MessageType.values()) {
		            if (a.value == value)
		                return a;
		        }
		        throw new IllegalArgumentException("Invalid id");
		    }
	};
	
	
	public static class RequestData{
		public String cmd;
		public String data;
		public String arg;
		public boolean success;
				
		public RequestData(String request) {
			try {
				JSONObject json = new JSONObject(request);
				
				cmd="";
				data="";
				arg="";
				if (json.has("command"))
					cmd = json.getString("command");
				if (json.has("data"))
					data = json.getString("data");
				if (json.has("arg"))
					arg = json.getString("arg");
				if (cmd==null || cmd.length()==0)
				{
					success = false;
					return;
				}
				success = true;

			} catch (Exception ex) {
				success = false;
			}
		}
	}

}
