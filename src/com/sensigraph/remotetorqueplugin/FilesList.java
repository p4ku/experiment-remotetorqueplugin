package com.sensigraph.remotetorqueplugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class FilesList extends Activity {

	private Singleton singleton = Singleton.getInstance();
	private List<MyFile> fileList = new ArrayList<MyFile>();
	private ContactCursorAdapter customAdapter;
	private ListView lvFiles;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.files_list);
		 lvFiles = (ListView) findViewById(R.id.lvFiles);
	        
		 String dir = singleton.writter.getDir();
		if (dir != null) {
			File root = new File(dir);
			ListDir(root);
		}

	}

	void ListDir(File f) {
		File[] files = f.listFiles();
		// sort by date
		Arrays.sort(files, new Comparator<File>() {
		    public int compare(File f1, File f2) {
		        return Long.compare(f2.lastModified(), f1.lastModified());
		    }
		});
		
		fileList.clear();
		for (File file : files) {
			fileList.add(new MyFile(file));
		}
		
		customAdapter = new ContactCursorAdapter(this, fileList);

		
		lvFiles.setAdapter(customAdapter);
	}
	
	
	public class MyFile{
		File file;
		String fullPath;
		String fileName;
		String filePath;
		long size;
		
		public MyFile(File f){
			this.file = f;
			this.filePath = file.getPath();
			this.fileName = file.getName();
			this.filePath = file.getParent();
			this.size = file.length();
		}
	}
	
	
	
	// *************************************************************
		// CursorAdapter
		// *************************************************************
		public class ContactCursorAdapter extends ArrayAdapter<MyFile> implements
				Filterable {

		

			private int selectedPos = -1; // initial value for not-selected

			public ContactCursorAdapter(Context context, List<MyFile> fileList) {
				super(context, 0, fileList);
			}

		

			@Override
		    public View getView(int position, View convertView, ViewGroup parent) {
		       // Get the data item for this position
		       MyFile file = getItem(position);    
		       // Check if an existing view is being reused, otherwise inflate the view
		       if (convertView == null) {
		          convertView = LayoutInflater.from(getContext()).inflate(R.layout.files_row, parent, false);
		       }
		       // Lookup view for data population
		       TextView fileName = (TextView) convertView
						.findViewById(R.id.fileName);
				TextView filePath = (TextView) convertView
						.findViewById(R.id.filePath);
				TextView tags = (TextView) convertView.findViewById(R.id.size);
				//TextView target = (TextView) view.findViewById(R.id.target_name);
				ImageView image = (ImageView) convertView.findViewById(R.id.icon);
				
				fileName.setText(file.fileName);
				filePath.setText(file.filePath);
				tags.setText(Long.toString(file.size));

				
	
		       // Return the completed view to render on screen
		       return convertView;
		   }
			

	
		}

}