package com.sensigraph.remotetorqueplugin;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.prowl.torque.remote.ITorqueService;
import org.prowl.torquescan.utils.PID;
import org.prowl.torquescan.utils.PIDAdapter;
import org.prowl.torquescan.utils.PIDComparator;

import com.sensigraph.remotetorqueplugin.Singleton.UIHandlerInterface;


public class PluginActivity extends Activity {
		
	private Handler handler;

	private ITorqueService torqueService;
	private static PluginActivity instance;
	

	TextView tvStatus;
	TextView tvInfo;
	TextView tvMessage;
	TextView tvError;
	Button bAction;
	
	private ListView list;
	private Vector<PID> pids = new Vector();
	private PIDAdapter listViewArrayAdapter;
	
	private String bStart = "Start Logging";
	private String bStop = "Stop Logging";
	
		
	private Singleton singleton = Singleton.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.main);
			// ListView element, obtained by id
			list = (ListView) findViewById(R.id.maintListView);
			bAction = (Button) findViewById(R.id.bAction);
			bAction.setText(bStart);

			tvStatus = (TextView) findViewById(R.id.tvStatus);
			tvInfo = (TextView) findViewById(R.id.tvInfo);
			tvMessage = (TextView) findViewById(R.id.tvMessage);
			tvError = (TextView) findViewById(R.id.tvError);

			tvStatus.setBackgroundColor(Color.LTGRAY);
			tvStatus.setTextColor(Color.BLACK);
			tvStatus.setText(Singleton.loggingOff);
			
			
			ActionBar actionBar = getActionBar();
			// actionBar.setBackgroundDrawable(new ColorDrawable(0xff1A519D));
			actionBar.setDisplayUseLogoEnabled(true);
			actionBar.setDisplayShowTitleEnabled(true);			
			actionBar.setDisplayShowHomeEnabled(true);
			
			
			Button bFilesList = (Button) findViewById(R.id.bFilesList);
			bFilesList.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(PluginActivity.this, FilesList.class);
				    startActivity(intent);					
				}
			});

			handler = new Handler();
			instance = this;
			singleton.context = this;

			singleton.setUIHandlerListener(new UIHandlerInterface() {
				
				@Override
				public void onHandle(final String obj, final Utils.UIMesasge type) {
					PluginActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							processMessage(obj, type);
						}
					});					
				}
			});
				
			singleton.init();
			
			// Button Stats/Stop logging
			bAction.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (!singleton.logging){
						String fileName = new SimpleDateFormat("yyyy_MM_dd_HHmmss'.csv'").format(new Date());
						singleton.startLogging(fileName);
					}
					else
						singleton.stopLogging();
				}
			});

		} catch (Exception e) {
			e.printStackTrace();			
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Bind to the torque service
		try {
			// Bind to the torque service
			Intent intent = new Intent();
			intent.setClassName("org.prowl.torque",
					"org.prowl.torque.remote.TorqueService");
			boolean successfulBind = bindService(intent, connection, 0);

			if (successfulBind) {
				
				
			}else{
				tvError.setText("No torqueService");
			}

		} catch (Exception e) {
			Toast.makeText(this, "onresume Error: " + e.toString(),
					Toast.LENGTH_LONG).show();;
		}
		
		//singleton.setupServer();

	}

	//
	// @Override
	protected void onPause() {
		super.onPause();
		
		
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.option, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_exit) {
			shutdownServer();
			finish();
			return (true);
		}
		if (item.getItemId() == R.id.action_connect) {
			singleton.setupServer();
			return (true);
		}
		if (item.getItemId() == R.id.action_disconnect) {
			singleton.destroyServer();			
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}
	
	 public ITorqueService getTorqueService() {
	      return torqueService;
	   }
	 
	 public static final PluginActivity getInstance() {
	      return instance;
	   }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		shutdownServer();
	}
	
	
	private void shutdownServer(){
		singleton.destroyServer();
		try {
			// updateTimer.cancel();
			unbindService(connection);
		} catch (Exception ex) {
			//Toast.makeText(this, "Error unbinding: " + ex.toString(),
			//		Toast.LENGTH_LONG).show();
		}				
	}
	
	
	/**
	    * Bits of service code. You usually won't need to change this.
	    */
	   private ServiceConnection connection = new ServiceConnection() {
	      public void onServiceConnected(ComponentName arg0, IBinder service) {
	         torqueService = ITorqueService.Stub.asInterface(service);
	         singleton.setTorqueService(torqueService);	       
	         setupList();
	      };
	      public void onServiceDisconnected(ComponentName name) {
	         torqueService = null;
	      };
	   };
	   

	  // Refresh list
	  public void refreshListItems() {
	      if (listViewArrayAdapter == null)
	         return;

	      try {
	         String[] mpids = torqueService.listAllPIDs();
	         String[] pidInfo = torqueService.getPIDInformation(mpids);
	         pids.clear();
	         for (int i = 0; i < mpids.length; i++) {
	            String[] info = pidInfo[i].split(",");
	            boolean found = false;	            
	            for (int j=0; j < singleton.dataRow.Pids.length; j++){
	            	String s = singleton.dataRow.Pids[j]+",0";
	            	if(s.equals(mpids[i])){
	            		found = true;
	            		break;
	            	}	            	
	            }
	            if (!found)
	            	continue;
	            
	            PID newPid = new PID(mpids[i]);
	            newPid.setFullName(info[0]);
	            newPid.setShortName(info[1]);
	            newPid.setUnit(info[2]);
	            newPid.setUserPid(false);
	            pids.add(newPid);
	         }
	      } catch(RemoteException e) {
	         
	      }

	      Collections.sort(pids, new PIDComparator());

	      handler.post(new Runnable() { public void run() {
	         for (PID pid: pids) {
	            listViewArrayAdapter.addPID(pid, true);//; checkForDuplicates)
	         }
	      }});
	   }
	

	  /**
	     * Create the list that we are going to show to the user containing PIDS from torque
	     **/
	   public void setupList() {

	      list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	      listViewArrayAdapter = new PIDAdapter(this, (Vector)pids.clone());
	      list.setAdapter(listViewArrayAdapter);

	      list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	         public void onItemClick(AdapterView<?> av, View v, int a, long b) {
	            Object[] dataToAdd = null;
	            PID pid = (PID) listViewArrayAdapter.getItem(a);
	            if (pid != null) {

	               // View in large update thingy?

	            }
	         }
	      });
	      refreshListItems();
	   }
	   

	
	  
	  // Process UI controls
	private void processMessage(String paramString, Utils.UIMesasge paramType) {
		if (paramString == null)
			return;
		else if (paramType == Utils.UIMesasge.MESSAGE) {
			this.tvMessage.setText(paramString);

		} else if (paramType == Utils.UIMesasge.STATUS) {
			this.tvStatus.setText(paramString);

		} else if (paramType == Utils.UIMesasge.INFO) {
			this.tvInfo.setText(paramString);

		} else if (paramType == Utils.UIMesasge.ERROR) {
			this.tvError.setText(paramString);

		} else if (paramType == Utils.UIMesasge.BUTTON_START) {
			bAction.setText(bStop);
			tvStatus.setBackgroundColor(Color.RED);
			tvStatus.setTextColor(Color.WHITE);
			tvStatus.setText(paramString);
			
		} else if (paramType == Utils.UIMesasge.BUTTON_STOP) {
			bAction.setText(bStart);
			tvStatus.setBackgroundColor(Color.LTGRAY);
			tvStatus.setTextColor(Color.BLACK);
			tvStatus.setText(paramString);
			
		}
	}
	 
	  
}
