package com.sensigraph.remotetorqueplugin;

import java.text.NumberFormat;

import org.prowl.torque.remote.ITorqueService;

public class DataRow {
	
	//GPS Time, Device Time, Longitude, Latitude,GPS Speed (Meters/second), Horizontal Dilution of Precision, Altitude, Bearing, 
	// G(x), G(y), G(z), G(calibrated),Engine RPM(rpm),Speed (OBD)(km/h),Throttle 
	//Position(Manifold)(%),Trip Distance(km),GPS vs OBD Speed difference(km/h),
	//Average trip speed(whilst moving only)(km/h)
	
	
	//Sat Apr 26 18:58:56 CEST 2014,26-kwi-2014 18:58:53.236,19.95118847116828,50.02834025770426,0.0,5.0,265.0,0.0,-0.31,8.68,2.02,-0.07,859,0,0,-,-,-

	public String Pids[] = { 
			"ff1005",// GPS Longitude
			"ff1006",// GPS Latitude 
			"ff1001",// GPS Speed
			"ff1239",// GPS Accuracy / horizontal dilution of precision
			"ff1010",// GPS Altitude
			"ff123b",// GPS Bearing
			"ff1220",// G x
			"ff1221",// G y
			"ff1222",// G z
			"ff1223",// G total
			"0c",    // RPM
			"0d",    // Speed OBD
			"11",    // Throttle
			"ff1204",// Trip distance
			"ff1237",// GPS vs obd speed diff
			"ff1263",// avg trip speed
			"ff1225",// torque
			"ff1226",// Hoursepower
			"04"     // engine load
		 };
	public String Descriptions[] = {
			"GPS Time", 
			"Device Time", 
			"Longitude", 
			"Latitude",
			"GPS Speed (Meters/second)", 
			"Horizontal Dilution of Precision", 
			"Altitude", 
			"Bearing", 
			"G(x)", 
			"G(y)", 
			"G(z)", 
			"G(calibrated)",
			"Engine RPM(rpm)",
			"Speed (OBD)(km/h)",
			"Throttle",			
			"Trip Distance(km)",
			"GPS vs OBD Speed difference(km/h)",
			"Average trip speed(whilst moving only)(km/h)",
			"Torque",
			"Hoursepower",
			"Engine load"
			};
	
	public ITorqueService torqueService=null;
	private NumberFormat nf = NumberFormat.getInstance();
	
	
	public String prepareRow(){
		if (torqueService==null)
			return null;
		String result = "";
		try {
			float[] data = torqueService.getPIDValues(Pids);
			for (int i = 0; i < data.length; i++) {
				result+= Float.toString(data[i]).replace(",", ".")+",";
				
				//result+=nf.format((Number)data[i]).replace(",", ".")+",";
			}
		} catch (Exception ex) {
			result = null;
		}
		if (result!=null && result.length()>0)
			result = result.substring(0, result.length()-1);

		return result;
		
	}
}
